  class Setlist {
    String titulo;
    int durMin, durMax, durTotal;
    var list = new List();
    
    Setlist(String titulo, int durmin, int durmax) {
      this.titulo = titulo;
      this.durMax = durmax;
      this.durMin = durmin;
      this.durTotal = 0;
    }

    bool checkDurMin() {
      bool n;
      n = this.durTotal >= this.durMin ? true : false;
      return n;
    }

    bool checkDurMax() {
      bool n;
      n = this.durTotal > this.durMax ? true : false;
      return n;
    }

    void addmus(Musica x) {
      bool message = true;
      if (this.checkDurMax()) {
        print('"' + x.title() + '" não foi adicionada - duração deste setlist extrapolaria o limite');
      } else {
      this.list.add(x);
      this.durTotal += x.duracao;
      this.checkDurMin();
      if (this.checkDurMin() && message) {
        print('Duração mínima do setlist alcançada');
        message = false; // Garante que a mensagem acima seja exibida apenas uma vez
      }
    }
  }
    void status() {
      String minseg = this.minSeg(), s;
      int n = this.list.length;
      s = n > 1 ? 's' : '';
      print('Setlist "$titulo"\nDuração: $minseg\n$n música$s');
      for (int i = 0; i < this.list.length; i++) {
        int n = i + 1;
        print('$n. ' + this.list[i].title());
      }
    }

  String minSeg() {
  int h, min, seg, n = this.durTotal;
  String ho, mi, se;
  h = n ~/ 3600;
  ho = h > 0 ? '$h:' : '';

  min = (n % 3600)~/60;
  mi = min > 9 ? '$min:' : '0$min:';

  seg = (n % 3600)%60;
  se = seg < 10 ? '0$seg' : '$seg' ;

  return '$ho$mi$se';
 }

  void recolocar(Musica x, int n) {
    this.list.add(list[n-1]);
  }

}

class Musica {
  int duracao, avaliacao;
  String titulo, artista, album, duracaoMinSeg;

  Musica(int duracao, String titulo, String artista, String album, [int avaliacao = 1]) {
    this.duracao = duracao;
    this.avaliacao = avaliacao;
    this.titulo = titulo;
    this.artista = artista;
    this.album = album;
  }

  String title() {
    String t = this.titulo;
    return t;
  }

  void status() {
    print('-------------------------------------------------------------------------');
    print('"' + this.titulo + '" - ' + this.artista);
    print('Álbum: ' + this.album);
    print('Avaliação: ' + this.avaliacao.toString());
    print('Duração: ' + this.minSeg());
    print('-------------------------------------------------------------------------');
  }
  String minSeg() {
  int h, min, seg, n = this.duracao;
  String ho, mi, se;
  h = n ~/ 3600;
  ho = h > 0 ? '$h:' : '';

  min = (n % 3600)~/60;
  mi = min > 9 ? '$min:' : '0$min:';

  seg = (n % 3600)%60;
  se = seg < 10 ? '0$seg' : '$seg' ;

return '$ho$mi$se';
}}

void main() {

  Musica m1 = Musica(301, 'Achei', 'Oscar Alves', 'Freggs', 5);
  Musica m2 = Musica(221, 'Bem', 'Oscar Alves', 'Freggs');
  Musica m3 = Musica(197, 'Cala-te', 'Oscar Alves', 'Freggs', 3);
  Musica m4 = Musica(233, 'Laranja', 'Oscar Alves', 'Freggs', 4);
  Musica m5 = Musica(200, 'Tchau', 'Oscar Alves', 'Freggs', 4);
  Musica m6 = Musica(301, 'Vai Tarde Demais Pra Quem Nunca Deveria Ter Chegado', 'Oscar Alves', 'Freggs', 5);

  Setlist s1 = Setlist('Première', 1200, 1300);

  m1.status();
  m2.status();
  m3.status();
  m4.status();
  m5.status();
  m6.status();

  s1.addmus(m1);
  s1.addmus(m2);
  s1.addmus(m3);
  s1.addmus(m4);
  s1.addmus(m5);
  s1.addmus(m6);

  s1.status();

}