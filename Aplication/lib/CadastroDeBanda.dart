import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

void main() => runApp(MaterialApp(
  home: CadastroDeBanda(),
));

class CadastroDeBanda extends StatefulWidget {
  @override
  _CadastroDeBandaState createState() => _CadastroDeBandaState();
}

class _CadastroDeBandaState extends State<CadastroDeBanda> {
  DateTime _dateTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(40, 10, 40, 0),
        child: ListView(
          children: <Widget>[
            Container(
              width: 200,
              height: 200,
              alignment: Alignment(0.0, 1.3),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('Imagens/logocircular.png',),
                  fit: BoxFit.fitHeight,
                ),
              ),
              child: Container(
                height: 56,
                width: 56,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color:  Colors.blue,
                  border: Border.all(
                    width: 4.0,
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(56),
                  ),
                ),
                child: SizedBox.expand(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    shape: CircleBorder(),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: 'Nome da Banda',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  )
              ),
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            SizedBox(height: 20,),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: 'Número de Contato',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  )
              ),
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            SizedBox(height: 20,),
            Row(
              children: <Widget>[
                Text(
                  'Data de Fundação',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(width: 15,),
                Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      left: BorderSide(color: Colors.blue, width: 4, style: BorderStyle.solid),
                      bottom: BorderSide(color: Colors.blue, width: 4, style: BorderStyle.solid),
                      top: BorderSide(color: Colors.blue, width: 4, style: BorderStyle.solid),
                      right: BorderSide(color: Colors.blue, width: 4, style: BorderStyle.solid),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(7),),
                  ),
                  child: FlatButton(
                    child: Text(_dateTime == null ? 'Escolha a data' : DateFormat('dd/MM/yyyy').format(_dateTime).toString(),
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                    onPressed: () {
                      showDatePicker(
                        context: context,
                        initialDate: _dateTime == null ? DateTime.now() : _dateTime,
                        firstDate: DateTime(1950),
                        lastDate: DateTime.now(),
                      ).then((date) {
                        setState(() {
                          _dateTime = date;
                        });
                      });
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 25,),
            Container(
              height: 80,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(5),),
              ),
              child: SizedBox.expand(
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Cadastrar Banda',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {},
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              height: 30,
              alignment: Alignment.center,
              child: FlatButton(
                child: Text(
                  'Cancelar',
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                  textAlign: TextAlign.center,
                ),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}