import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';

void main() => runApp(MaterialApp(
  home: Repertorio(),
));

class Repertorio extends StatefulWidget {
  @override
  _RepertorioState createState() => _RepertorioState();
}

class _RepertorioState extends State<Repertorio>{
  var time = new MaskedTextController(mask: '00:00');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Repertório",
          style: TextStyle(fontSize: 25),
        ),
        backgroundColor: Colors.black87,
        centerTitle: true,
      ),
      drawer: new Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 125,
              child: DrawerHeader(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 25, 0, 0),
                  child: Text(
                    "Band Manager",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 35,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                  color: Colors.black,
                ),
              ),
            ),
            ListTile(
              title: Text("Tela 1"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text("Tela 2"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(40, 10, 40, 0),
        child: ListView(
          children: <Widget>[
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: 'Título da Música',
                  labelStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  )
              ),
              style: TextStyle(fontSize: 20),
              inputFormatters: [LengthLimitingTextInputFormatter(32),],
            ), // Título da Música
            SizedBox(height: 10,),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: 'Álbum',
                  labelStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  )
              ),
              style: TextStyle(fontSize: 20),
              inputFormatters: [LengthLimitingTextInputFormatter(32),],
            ), // Álbum
            SizedBox(height: 10,),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: 'Compositor(es)',
                  labelStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  )
              ),
              style: TextStyle(fontSize: 20),
              inputFormatters: [LengthLimitingTextInputFormatter(32),],
            ), // Compositores
            SizedBox(height: 10,),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Duração da Música',
                labelStyle: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 20,
                ),
              ),
              style: TextStyle(fontSize: 20,),
              inputFormatters: [LengthLimitingTextInputFormatter(5)],
              controller: time,
            ), // Duração da Música
            SizedBox(height: 25,),
            Container(
              height: 80,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.all(Radius.circular(5),),
              ),
              child: SizedBox.expand(
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Salvar Música',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {},
                ),
              ),
            ), // Botão para salvar música
            SizedBox(height: 10,),
            Container(
              height: 30,
              alignment: Alignment.center,
              child: FlatButton(
                child: Text(
                  'Cancelar',
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                  textAlign: TextAlign.center,
                ),
                onPressed: () {},
              ),
            ), // Botão 'Cancelar'
          ],
        ),
      ),
    );
  }
}
