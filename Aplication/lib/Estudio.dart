import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Estudio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black38,
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      backgroundColor: Colors.teal,
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Cadastro de estúdio",
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.text,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "Nome do estúdio:",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.text,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "Dono",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.number,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "CNPJ",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.emailAddress,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "E-mail",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.phone,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "Telefone",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.emailAddress,
                style: new TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                    labelText: "Facebook",
                    labelStyle: TextStyle(color: Colors.white)),
              ),
              SizedBox(
                height:20,
              ),
              Container(
                height: 60,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.2,1],
                    colors: [
                      Color(0XFF054F77),
                      Color(0XFFFF8C00),
                    ],
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(25),
                  ),
                ),
                child: SizedBox.expand(
                  child: FlatButton(
                    child: Text(
                      "Entrar",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () => {},
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
